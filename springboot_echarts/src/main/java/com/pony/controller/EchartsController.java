package com.pony.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author 巅峰小词典
 * @description
 * @date 2021/5/21
 * @project springboot_echarts
 */
@RestController
public class EchartsController {

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String sayHello() {
		return "Hello Spring Boot!";
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ModelAndView testDemo() {
		// 跟templates文件夹下的test.html名字一样，返回这个界面
		return new ModelAndView("test");
	}

	@RequestMapping(value = "/demo", method = RequestMethod.GET)
	public ModelAndView demoStat() {
		// 跟templates文件夹下的demo.html名字一样，返回这个界面
		return new ModelAndView("demo");
	}

}
