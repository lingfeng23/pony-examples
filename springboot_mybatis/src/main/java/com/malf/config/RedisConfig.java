package com.malf.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author malf
 * @description Redis 配置类，设置Redis的Template
 * @date 2021/5/23
 * @project springboot_mybatis
 */
@Configuration
public class RedisConfig {

	// redisTemplate注入到Spring容器
	@Bean
	public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory factory) {
		RedisTemplate<String, String> redisTemplate = new RedisTemplate<>();
		RedisSerializer<String> redisSerializer = new StringRedisSerializer();
		redisTemplate.setConnectionFactory(factory);
		// key序列化
		redisTemplate.setKeySerializer(redisSerializer);
		// value序列化
		redisTemplate.setValueSerializer(redisSerializer);
		// value hashmap序列化
		redisTemplate.setHashKeySerializer(redisSerializer);
		// key hashmap序列化
		redisTemplate.setHashValueSerializer(redisSerializer);
		return redisTemplate;
	}

}
