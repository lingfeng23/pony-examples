package com.malf.controller;

import com.malf.entity.NBAPlayer;
import com.malf.service.NBAPlayerService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 巅峰小词典
 * @description
 * @date 2021/5/20
 * @project springboot_mybatis
 */
@RestController
@RequestMapping("/nba")
public class NBAPlayerController {

	@Resource
	private NBAPlayerService nbaPlayerService;

	@RequestMapping("getPlayer/{id}")
	public String getPlayer(@PathVariable int id) {
		return nbaPlayerService.selectById(id).toString();
	}

	@RequestMapping("list")
	public List<NBAPlayer> list() {
		return nbaPlayerService.list();
	}

}
