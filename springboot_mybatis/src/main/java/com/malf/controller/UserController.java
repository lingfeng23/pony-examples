package com.malf.controller;

import com.malf.service.UserService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 巅峰小词典
 * @description
 * @date 2021/5/20
 * @project springboot_mybatis
 */
@RestController
@RequestMapping("/user")
public class UserController {

	@Resource
	private UserService userService;

	@RequestMapping("getUser/{id}")
	public String GetUser(@PathVariable int id) {
		return userService.selectById(id).toString();
	}

}
