package com.malf.service;

import com.malf.entity.NBAPlayer;

import java.util.List;

/**
 * @author 巅峰小词典
 * @description
 * @date 2021/5/20
 * @project springboot_mybatis
 */
public interface NBAPlayerService {

	public NBAPlayer selectById(int id);

	public List<NBAPlayer> list();

}
