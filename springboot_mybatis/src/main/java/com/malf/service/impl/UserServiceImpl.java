package com.malf.service.impl;

import com.malf.entity.User;
import com.malf.dao.UserMapper;
import com.malf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 巅峰小词典
 * @description
 * @date 2021/5/20
 * @project springboot_mybatis
 */
@Service
public class UserServiceImpl implements UserService {

	@Resource
	UserMapper userMapper;

	public User selectById(int id){
		return userMapper.selectById(id);
	}

}
