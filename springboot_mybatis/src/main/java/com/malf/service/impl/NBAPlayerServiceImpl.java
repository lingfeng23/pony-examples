package com.malf.service.impl;

import com.malf.dao.NBAPlayerMapper;
import com.malf.entity.NBAPlayer;
import com.malf.service.NBAPlayerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 巅峰小词典
 * @description
 * @date 2021/5/20
 * @project springboot_mybatis
 */
@Service
public class NBAPlayerServiceImpl implements NBAPlayerService {

	@Resource
	NBAPlayerMapper nbaPlayerMapper;

	public NBAPlayer selectById(int id) {
		return nbaPlayerMapper.selectById(id);
	}

	public List<NBAPlayer> list() {
		return nbaPlayerMapper.list();
	}

}
