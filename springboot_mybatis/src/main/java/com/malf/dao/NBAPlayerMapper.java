package com.malf.dao;

import com.malf.entity.NBAPlayer;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 巅峰小词典
 * @description
 * @date 2021/5/20
 * @project springboot_mybatis
 */
@Repository
public interface NBAPlayerMapper {

	NBAPlayer selectById(int id);

	List<NBAPlayer> list();

}
