package com.malf.dao;

import com.malf.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author 巅峰小词典
 * @description
 * @date 2021/5/20
 * @project springboot_mybatis
 */
@Repository
public interface UserMapper {

	User selectById(int id);

}
