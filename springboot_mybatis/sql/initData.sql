create table user
(
    id       int auto_increment
        primary key,
    userName varchar(32) not null,
    passWord varchar(50) not null,
    realName varchar(32) null
)
    charset = utf8;

create table nba_player
(
    id int auto_increment comment '主键',
    name varchar(50) null comment '姓名',
    country varchar(100) null comment '国籍',
    team varchar(100) null comment '球队',
    birthday datetime null comment '生日',
    constraint nba_player_pk
        primary key (id)
)
    comment 'NBA 球星';