package com.pony.shiro;

import cn.hutool.core.bean.BeanUtil;
import com.pony.entity.User;
import com.pony.service.UserService;
import com.pony.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author malf
 * @description shiro进行登录或者权限校验的逻辑所在
 * @date 2021/5/22
 * @project springboot_blog
 */
@Slf4j
@Component
public class AccountRealm extends AuthorizingRealm {

	@Resource
	JwtUtils jwtUtils;
	@Resource
	UserService userService;

	// 让realm支持jwt的凭证校验
	@Override
	public boolean supports(AuthenticationToken token) {
		return token instanceof JwtToken;
	}

	// 权限校验
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		return null;
	}

	// 登录认证校验
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		JwtToken jwt = (JwtToken) token;
		log.info("jwt----------------->{}", jwt);
		String userId = jwtUtils.getClaimByToken((String) jwt.getPrincipal()).getSubject();
		User user = userService.getById(Long.parseLong(userId));
		if (user == null) {
			throw new UnknownAccountException("账户不存在！");
		}
		if (user.getStatus() == -1) {
			throw new LockedAccountException("账户已被锁定！");
		}
		AccountProfile profile = new AccountProfile();
		BeanUtil.copyProperties(user, profile);
		log.info("profile----------------->{}", profile.toString());
		return new SimpleAuthenticationInfo(profile, jwt.getCredentials(), getName());
	}

}
