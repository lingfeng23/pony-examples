package com.pony.shiro;

import lombok.Data;

import java.io.Serializable;

/**
 * @author malf
 * @description 登录成功之后返回的一个用户信息的载体
 * @date 2021/5/22
 * @project springboot_blog
 */
@Data
public class AccountProfile implements Serializable {

	private Long id;
	private String username;
	private String avatar;

}
