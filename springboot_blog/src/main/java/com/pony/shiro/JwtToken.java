package com.pony.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author malf
 * @description shiro默认supports的是UsernamePasswordToken，我们现在采用了jwt的方式，
 * 所以这里自定义一个JwtToken，来完成shiro的supports方法。
 * @date 2021/5/22
 * @project springboot_blog
 */
public class JwtToken implements AuthenticationToken {

	private String token;

	public JwtToken(String token) {
		this.token = token;
	}

	@Override
	public Object getPrincipal() {
		return token;
	}

	@Override
	public Object getCredentials() {
		return token;
	}

}
