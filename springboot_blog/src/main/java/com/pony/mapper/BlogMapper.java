package com.pony.mapper;

import com.pony.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pony
 * @since 2021-05-22
 */
public interface BlogMapper extends BaseMapper<Blog> {

}
