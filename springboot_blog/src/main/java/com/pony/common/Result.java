package com.pony.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author malf
 * @description 用于异步统一返回的结果封装。
 * @date 2021/5/22
 * @project springboot_blog
 */
@Data
public class Result implements Serializable {

    private String code;    // 是否成功
    private String message; // 结果消息
    private Object data;    // 结果数据

    public static Result success(Object data) {
        Result result = new Result();
        result.setCode("0");
        result.setData(data);
        result.setMessage("操作成功");
        return result;
    }

    public static Result success(String message, Object data) {
        Result result = new Result();
        result.setCode("0");
        result.setData(data);
        result.setMessage(message);
        return result;
    }

    public static Result fail(String message) {
        Result result = new Result();
        result.setCode("-1");
        result.setData(null);
        result.setMessage(message);
        return result;
    }

    public static Result fail(String message, Object data) {
        Result result = new Result();
        result.setCode("-1");
        result.setData(data);
        result.setMessage(message);
        return result;
    }

    public static Result fail(String code, String message, Object data) {
        Result result = new Result();
        result.setCode(code);
        result.setData(data);
        result.setMessage(message);
        return result;
    }
    
}
