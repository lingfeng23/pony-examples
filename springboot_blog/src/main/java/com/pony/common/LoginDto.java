package com.pony.common;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author malf
 * @description
 * @date 2021/5/22
 * @project springboot_blog
 */
@Data
public class LoginDto {

	@NotBlank(message = "昵称不能为空")
	private String username;

	@NotBlank(message = "密码不能为空")
	private String password;

}
