package com.pony.controller;

import com.pony.entity.User;
import com.pony.service.UserService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author pony
 * @since 2021-05-22
 */
@RestController
@RequestMapping("/user")
public class UserController {

	@Resource
	UserService userService;

	@GetMapping("/{id}")
	public Object test(@PathVariable("id") Long id) {
		return userService.getById(id);
	}

	/**
	 * 测试实体校验
	 *
	 * @param user
	 * @return
	 */
	@PostMapping("/save")
	public Object testUser(@Validated @RequestBody User user) {
		return user.toString();
	}

}
