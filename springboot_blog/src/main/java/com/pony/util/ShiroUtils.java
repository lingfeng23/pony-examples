package com.pony.util;

import com.pony.shiro.AccountProfile;
import org.apache.shiro.SecurityUtils;

/**
 * @author malf
 * @description
 * @date 2021/5/22
 * @project springboot_blog
 */
public class ShiroUtils {

	public static AccountProfile getProfile() {
		return (AccountProfile) SecurityUtils.getSubject().getPrincipal();
	}

}
