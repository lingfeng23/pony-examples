package com.pony.service.impl;

import com.pony.entity.Blog;
import com.pony.mapper.BlogMapper;
import com.pony.service.BlogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pony
 * @since 2021-05-22
 */
@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements BlogService {

}
