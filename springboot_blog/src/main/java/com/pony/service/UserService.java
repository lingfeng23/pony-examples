package com.pony.service;

import com.pony.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pony
 * @since 2021-05-22
 */
public interface UserService extends IService<User> {

}
