-- auto-generated definition
create table user
(
    id          varchar(32)  not null
        primary key,
    nickname    varchar(255) not null,
    mail        varchar(255) not null,
    password    varchar(255) not null,
    description varchar(255) null,
    update_time datetime     null,
    create_time datetime     null
)
    charset = utf8mb4;

