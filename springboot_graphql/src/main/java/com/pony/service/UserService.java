package com.pony.service;

import com.pony.common.CommonUtils;
import com.pony.common.Result;
import com.pony.dao.UserMapper;
import com.pony.entity.AddUserInput;
import com.pony.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author malf
 * @description
 * @date 2021/5/24
 * @project springboot_graphql
 */
@Service
public class UserService {

	private static final Logger logger = LogManager.getLogger(UserService.class.getName());

	@Resource
	private UserMapper userMapper;

	public User getUserByNickname(String nickname) {
		logger.info("Service ==> getUserByNickname");
		return userMapper.getUserByNickname(nickname);
	}

	public List<User> listUsers() {
		logger.info("Service ==> listUsers");
		return userMapper.listUsers();
	}

	public Result addUser(String mail, String nickname, String password, String description) {
		logger.info("Service ==> getUser");
		User userDb = userMapper.getUserByNickname(nickname);
		if (null != userDb) {
			return new Result(-110, "用户昵称存在");
		}
		User addUser = new User();
		addUser.setId(CommonUtils.getUUID());
		addUser.setMail(mail);
		addUser.setNickname(nickname);
		addUser.setPassword(password);
		addUser.setDescription(description);
		userMapper.addUser(addUser);
		return new Result(100, "Success");
	}

	public Result deleteUser(String id) {
		logger.info("Service ==> deleteUser");
		User user = userMapper.getUserById(id);
		if (null == user) {
			return new Result(-500, "数据不存在");
		}
		userMapper.deleteUser(id);
		return new Result(100, "Success");
	}

	public User updateUser(String id, String mail, String nickname, String description) {
		logger.info("Service ==> updateUser");
		User updateUser = new User();
		updateUser.setId(id);
		updateUser.setMail(mail);
		updateUser.setNickname(nickname);
		updateUser.setDescription(description);
		userMapper.updateUser(updateUser);
		return updateUser;
	}

	public User addUserInput(AddUserInput addUserInput) {
		logger.info("Service ==> addUserInput");
		User addUser = new User();
		addUser.setId(CommonUtils.getUUID());
		addUser.setMail(addUserInput.getMail());
		addUser.setNickname(addUserInput.getNickname());
		addUser.setPassword(addUserInput.getPassword());
		addUser.setDescription(addUserInput.getDescription());
		userMapper.addUser(addUser);
		return addUser;
	}

}
