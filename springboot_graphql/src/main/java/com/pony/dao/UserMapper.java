package com.pony.dao;

import com.pony.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author malf
 * @description
 * @date 2021/5/24
 * @project springboot_graphql
 */
@Mapper
public interface UserMapper {

	User getUserById(String id);

	User getUserByNickname(String nickname);

	List<User> listUsers();

	void addUser(User user);

	void deleteUser(String id);

	void updateUser(User user);

}
