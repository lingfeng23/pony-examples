package com.pony.entity;

import lombok.Data;

/**
 * @author malf
 * @description
 * @date 2021/5/24
 * @project springboot_graphql
 */
@Data
public class User {

	private String id;
	private String nickname;
	private String password;
	private String mail;
	private String description;
	private String createTime;
	private String updateTime;

}
