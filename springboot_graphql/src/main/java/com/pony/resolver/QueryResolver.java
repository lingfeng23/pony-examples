package com.pony.resolver;

import com.pony.entity.User;
import com.pony.service.UserService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author malf
 * @description
 * @date 2021/5/24
 * @project springboot_graphql
 */
@Component
public class QueryResolver implements GraphQLQueryResolver {

	private static final Logger logger = LogManager.getLogger(QueryResolver.class);

	@Resource
	private UserService userService;

	public User user(String nickname) {
		logger.info("Query Resolver ==> user");
		logger.info("params: nickname:{}", nickname);
		return userService.getUserByNickname(nickname);
	}

	public List<User> users() {
		logger.info("Query Resolver ==> users");
		return userService.listUsers();
	}

}
