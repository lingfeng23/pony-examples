package com.pony.common;

import lombok.Data;

/**
 * @author malf
 * @description 返回结果类
 * @date 2021/5/24
 * @project springboot_graphql
 */
@Data
public class Result {

	private Integer code;
	private String message;

	public Result(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

}
