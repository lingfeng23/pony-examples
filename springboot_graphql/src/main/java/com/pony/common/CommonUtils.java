package com.pony.common;

import java.util.UUID;

/**
 * @author malf
 * @description 公用工具类
 * @date 2021/5/24
 * @project springboot_graphql
 */
public final class CommonUtils {

	private CommonUtils() {

	}

	// 生成随机主键
	public static String getUUID() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

}
