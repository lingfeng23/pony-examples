package com.pony.Fadapter;

/**
 * @author malf
 * @Description 目标接口
 * @project patternDesign
 * @since 2020/8/23
 */
public interface Targetable {
	void editTextFile();
	void editWordFile();
}
