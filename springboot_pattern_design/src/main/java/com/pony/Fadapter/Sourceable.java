package com.pony.Fadapter;

/**
 * @author malf
 * @Description 公共接口
 * @project patternDesign
 * @since 2020/8/23
 */
public interface Sourceable {
	void editTextFile();
	void editWordFile();
}
