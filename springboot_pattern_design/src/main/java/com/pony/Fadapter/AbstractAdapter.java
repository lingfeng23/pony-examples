package com.pony.Fadapter;

/**
 * @author malf
 * @Description 抽象适配器实现公共接口
 * @project patternDesign
 * @since 2020/8/23
 */
public abstract class AbstractAdapter implements Sourceable {
	@Override
	public void editTextFile() {

	}

	@Override
	public void editWordFile() {

	}
}
