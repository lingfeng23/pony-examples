package com.pony.Mstrategy;

/**
 * @author malf
 * @Description 出行方式策略
 * @project patternDesign
 * @since 2020/8/23
 */
public interface TravelStrategy {
	void travelMode();
}
