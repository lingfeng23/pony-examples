package com.pony.Afactory;

/**
 * @author malf
 * @Description 华为手机
 * @project patternDesign
 * @since 2020/8/23
 */
public class HuaWei implements Phone {
	@Override
	public String brand() {
		return "这是一部华为手机。";
	}
}
