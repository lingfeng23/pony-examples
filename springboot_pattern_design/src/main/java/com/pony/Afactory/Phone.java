package com.pony.Afactory;

/**
 * @author malf
 * @Description 手机接口
 * @project patternDesign
 * @since 2020/8/23
 */
public interface Phone {
	String brand();
}
