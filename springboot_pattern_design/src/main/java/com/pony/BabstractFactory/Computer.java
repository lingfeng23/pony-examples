package com.pony.BabstractFactory;

/**
 * @author malf
 * @Description 电脑接口
 * @project patternDesign
 * @since 2020/8/23
 */
public interface Computer {
	String internet();
}
