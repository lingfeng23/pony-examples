package com.pony.Tstate;

/**
 * @author malf
 * @Description 抽象状态
 * @project patternDesign
 * @since 2020/8/24
 */
public abstract class AbstractState {
	public abstract void action(Context context);
}
