package com.pony.Hproxy;

/**
 * @author malf
 * @Description 公司接口
 * @project patternDesign
 * @since 2020/8/23
 */
public interface Company {
	void findWorker(String title);
}
