package com.pony.Rcommand;

/**
 * @author malf
 * @Description 执行命令的接口
 * @project patternDesign
 * @since 2020/8/24
 */
public interface Command {
	public void execute(String command);
}
