package com.pony.Jbridge;

/**
 * @author malf
 * @Description 驱动器接口
 * @project patternDesign
 * @since 2020/8/23
 */
public interface Driver {
	void executeSQL();
}
