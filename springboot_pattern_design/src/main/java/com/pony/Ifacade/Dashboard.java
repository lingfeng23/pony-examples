package com.pony.Ifacade;

/**
 * @author malf
 * @Description 仪表盘
 * @project patternDesign
 * @since 2020/8/23
 */
public class Dashboard {
	public void startup() {
		System.out.println("仪表盘启动。");
	}
	public void shutdown() {
		System.out.println("仪表盘关闭。");
	}
}
