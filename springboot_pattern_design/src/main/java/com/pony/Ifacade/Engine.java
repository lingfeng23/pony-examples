package com.pony.Ifacade;

/**
 * @author malf
 * @Description 发动机
 * @project patternDesign
 * @since 2020/8/23
 */
public class Engine {
	public void startup() {
		System.out.println("发动机启动。");
	}
	public void shutdown() {
		System.out.println("发动机关闭。");
	}
}
