模板方法（Template Method）模式定义了一个算法框架，并通过继承的方式将算法的实现延
迟到子类中，使得子类可以在不改变算法框架及其流程的前提下重新定义该算法在某些特定环节
的实现，是一种类行为型模式。

该模式在抽象类中定义了算法的结构并实现了公共部分算法，在子类中实现可变的部分并根据不
同的业务需求实现不同的扩展。模板方法模式的优点在于其在父类（抽象类）中定义了算法的框
架以保障算法的稳定性，同时在父类中实现了算法公共部分的方法来保障代码的复用；将部分算
法部分延迟到子类中实现，因此子类可以通过继承的方式来扩展或重新定义算法的功能而不影
响算法的稳定性，符合开闭原则。

模板方法模式需要注意抽象类与具体子类之间的协作，在具体使用时包含以下主要角色。
#####◎ 抽象类（Abstract Class）：定义了算法的框架，由基本方法和模板方法组成。基本方法定义了算法有哪些环节，模板方法定义了算法各个环节执行的流程。
#####◎ 具体子类（Concrete Class）：对在抽象类中定义的算法根据需求进行不同的实现。