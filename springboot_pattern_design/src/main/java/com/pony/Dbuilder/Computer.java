package com.pony.Dbuilder;

/**
 * @author malf
 * @Description 需要生产的产品：电脑
 * @project patternDesign
 * @since 2020/8/23
 */
public class Computer {
	private String cpu;
	private String memory;
	private String disk;

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getDisk() {
		return disk;
	}

	public void setDisk(String disk) {
		this.disk = disk;
	}
}
