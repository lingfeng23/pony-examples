package com.pony.Gdecorator;

/**
 * @author malf
 * @Description 被装饰者
 * @project patternDesign
 * @since 2020/8/23
 */
public interface Sourceable {
	public void createComputer();
}
