### 《重学Java设计模式》

#### 创建者模式

##### Afactory(工厂方法模式)

##### BabstractFactory(抽象工厂模式)

##### Csingleton(单例模式)

##### Dbuilder(建造者模式)

##### Eprototype(原型模式)

#### 结构型模式

##### Fadapter(适配器模式)

##### Gdecorator(装饰器模式)

##### Hproxy(代理模式)

##### Ifacade(外观模式)

##### Jbridge(桥接模式)

##### Kcomposite(组合模式)

##### Lflyweight(享元模式)

#### 行为模式

##### Mstrategy(策略模式)

##### Ntemplate(模板模式)

##### Oobserver(观察者模式)

##### Piterator(迭代器模式)

##### QchainOfResponsibility(责任链模式)

##### Rcommand(命令模式)

##### Smemento(备忘录模式)

##### Tstate(状态模式)

##### Uvistor(访问者模式)

##### Vmediator(中介者模式)

##### Winterpretor(解释器模式)

