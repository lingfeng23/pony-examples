# 创建设置类
class Settings():
  # 存储《外星人入侵》的所有设置的类
  def __init__(self):
    # 初始化游戏的设置
    # 屏幕设置
    self.screen_width = 900
    self.screen_height = 600
    self.bg_color = (230, 230, 230)

    # 飞船设置
    self.ship_limit = 3

    # 子弹设置
    self.bullet_width = 3
    self.bullet_height = 15
    self.bullet_color = 60, 60, 60
    self.bullets_allowed = 3

    # 外星人设置
    self.fleet_drop_speed = 10

    # How quickly the game speeds up.
    self.speedup_scale = 1.1
    # How quickly the alien point values increase.
    self.score_scale = 1.5

    self.initialize_dynamic_settings()

  def initialize_dynamic_settings(self):
    """Initialize settings that change throughout the game."""
    self.ship_speed_factor = 1.5
    self.bullet_speed_factor = 3
    self.alien_speed_factor = 1

    # Scoring.
    self.alien_points = 50

    # fleet_direction 为1表示向右移，为-1表示向左移
    self.fleet_direction = 1

  def increase_speed(self):
    """Increase speed settings and alien point values."""
    self.ship_speed_factor *= self.speedup_scale
    self.bullet_speed_factor *= self.speedup_scale
    self.alien_speed_factor *= self.speedup_scale

    self.alien_points = int(self.alien_points * self.score_scale)
